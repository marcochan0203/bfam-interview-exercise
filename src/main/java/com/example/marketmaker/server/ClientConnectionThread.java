package com.example.marketmaker.server;

import com.example.marketmaker.server.models.Command;
import com.example.marketmaker.server.services.impl.QuoteCalculationEngineImpl;
import com.example.marketmaker.server.services.impl.QuoteServiceImpl;
import com.example.marketmaker.server.services.impl.ReferencePriceSourceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class ClientConnectionThread implements Runnable {
    private final Socket socket;
    private final QuoteServiceImpl quoteService;

    ClientConnectionThread(Socket clientSocket) {
        this.socket = clientSocket;
        this.quoteService = new QuoteServiceImpl(new QuoteCalculationEngineImpl(0.01), new ReferencePriceSourceImpl());
    }

    @Override
    public void run() {
        BufferedReader input;
        PrintWriter output;
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(socket.getOutputStream());

        } catch (IOException e) {
            System.out.printf("[Error] Cannot get the input stream for %s%n", socket.getRemoteSocketAddress());
            return;
        }

        while (true) {
            try {
                final String text = input.readLine();
                if ((text == null) || text.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    double quotedPrice = quoteService.getQuote(Command.fromString(text));

                    output.println(quotedPrice);
                    output.flush();
                }
            } catch (SocketException socketException) {
                System.out.println("Socket = " + socket.getLocalSocketAddress() + " is closed " + socketException.getMessage());
                return;

            } catch (IOException | IllegalStateException e) {
                e.printStackTrace();
                output.println(e);
                output.flush();
            }
        }
    }
}
