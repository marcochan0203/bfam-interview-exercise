package com.example.marketmaker.server;

import com.example.marketmaker.server.services.impl.ReferencePriceSourceListenerImpl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class ServerApplication {
    public static final ConcurrentHashMap<Integer, ReferencePriceSourceListenerImpl> referencePriceSourceListener = new ConcurrentHashMap<>();
    public static final Random randomHelper = new Random();

    ServerApplication(int port) throws IOException {
        startServer(port);
    }

    public static void main(String[] args) throws IOException {
        startServer(12345);
    }

    private static void startServer(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Server started");
        while (true) {
            Socket socket = serverSocket.accept();
            System.out.printf("New connection received from [Address = %s]%n", socket.getLocalSocketAddress());

            Thread thread = new Thread(new ClientConnectionThread(socket));
            thread.start();
        }
    }
}

