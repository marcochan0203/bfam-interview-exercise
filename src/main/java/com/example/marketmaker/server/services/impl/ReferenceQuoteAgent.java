package com.example.marketmaker.server.services.impl;

import static com.example.marketmaker.server.ServerApplication.randomHelper;

public class ReferenceQuoteAgent implements Runnable {

    private ReferencePriceSourceListenerImpl priceSourceListener;

    public ReferenceQuoteAgent(ReferencePriceSourceListenerImpl priceSourceListener) {
        this.priceSourceListener = priceSourceListener;
    }

    @Override
    public void run() {
        //      Assume for getting an initial price of a security required by longer random time
        //      Assume for getting new updated price of a security is required by shorter random time
        while (true) {
            try {
                if (priceSourceListener.getPrice() != null) {
                    // New changes on the price
                    Thread.sleep(randomHelper.nextInt(5000));
                    int movement = randomHelper.nextInt(3) - 1;
                    double newPrice = priceSourceListener.getPrice() + movement;

                    System.out.println("Got new updated price for security = " + priceSourceListener.getSecurity() + " price = " + newPrice + " (movement = " + movement + ")");
                    priceSourceListener.referencePriceChanged(priceSourceListener.getSecurity(), newPrice);
                } else {
                    // new price
                    Thread.sleep(randomHelper.nextInt(5000) + 5000);
                    int newPrice = randomHelper.nextInt(100);
                    if (newPrice != priceSourceListener.getSecurity()) {
                        System.out.println("Got new initial price for security = " + priceSourceListener.getSecurity() + " price = " + newPrice);
                        priceSourceListener.referencePriceChanged(priceSourceListener.getSecurity(), newPrice);
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
