package com.example.marketmaker.server.services.impl;

import com.example.marketmaker.server.models.Command;
import com.example.marketmaker.server.models.Way;
import com.example.marketmaker.server.services.QuoteCalculationEngine;
import com.example.marketmaker.server.services.ReferencePriceSource;

public class QuoteServiceImpl {
    private final QuoteCalculationEngine quoteCalculationEngine;
    private final ReferencePriceSource referencePriceSource;

    public QuoteServiceImpl(QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource) {
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
    }

    public double getQuote(Command command) {
        referencePriceSource.subscribe(new ReferencePriceSourceListenerImpl(command.getSecurity(), null));
        double referencePrice = referencePriceSource.get(command.getSecurity());
        return quoteCalculationEngine.calculateQuotePrice(
                command.getSecurity(),
                referencePrice,
                Way.BUY.equals(command.getWay()),
                command.getQuantity()
        );

    }
}
