package com.example.marketmaker.server.services.impl;

import com.example.marketmaker.server.services.ReferencePriceSource;
import com.example.marketmaker.server.services.ReferencePriceSourceListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.example.marketmaker.server.ServerApplication.referencePriceSourceListener;

public class ReferencePriceSourceImpl implements ReferencePriceSource {
    ExecutorService executorService = Executors.newCachedThreadPool();

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        ReferencePriceSourceListenerImpl priceSourceListener = (ReferencePriceSourceListenerImpl) listener;
        if (!referencePriceSourceListener.containsKey(priceSourceListener.getSecurity())) {
            Thread referenceQuoteAgent = new Thread(new ReferenceQuoteAgent(priceSourceListener));
            executorService.submit(referenceQuoteAgent);
            System.out.println("Start listening for reference price for security = " + priceSourceListener.getSecurity());
            referencePriceSourceListener.put(priceSourceListener.getSecurity(), priceSourceListener);
        }
    }

    @Override
    public double get(int securityId) {
        if (referencePriceSourceListener.get(securityId).getPrice() == null) {
            synchronized (referencePriceSourceListener.get(securityId)) {
                try {
                    referencePriceSourceListener.get(securityId).wait();
                    System.out.println("Got reference price for security = " + securityId);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return referencePriceSourceListener.get(securityId).getPrice();
    }
}
