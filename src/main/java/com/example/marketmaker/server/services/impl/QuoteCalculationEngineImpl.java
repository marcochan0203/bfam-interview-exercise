package com.example.marketmaker.server.services.impl;

import com.example.marketmaker.server.services.QuoteCalculationEngine;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {


    private double spread;

    public QuoteCalculationEngineImpl(double spread) {
        this.spread = spread;
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        if (buy) {
            return (referencePrice + referencePrice * spread) * quantity;
        } else {
            return (referencePrice - referencePrice * spread) * quantity;
        }
    }
}
