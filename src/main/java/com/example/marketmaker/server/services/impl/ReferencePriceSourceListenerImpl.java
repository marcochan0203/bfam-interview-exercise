package com.example.marketmaker.server.services.impl;

import com.example.marketmaker.server.services.ReferencePriceSourceListener;


public class ReferencePriceSourceListenerImpl implements ReferencePriceSourceListener {

    private final int security;
    private Double price;

    ReferencePriceSourceListenerImpl(int security, Double price) {
        this.security = security;
        this.price = price;
    }

    @Override
    public synchronized void referencePriceChanged(int securityId, double price) {
        this.price = price;
        this.notifyAll();
    }


    synchronized Double getPrice() {
        return price;
    }

    int getSecurity() {
        return security;
    }
}
