package com.example.marketmaker.server.models;

public enum Way {
    BUY,
    SELL
}
