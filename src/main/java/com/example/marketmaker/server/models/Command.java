package com.example.marketmaker.server.models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Command {
    private final int security;
    private final Way way;
    private final int quantity;

    public static Command fromString(String text) {
        Pattern pattern = Pattern.compile("(?<security>\\d+)\\s(?<way>(BUY|SELL))\\s(?<quantity>\\d+)");
        Matcher matcher = pattern.matcher(text);

        if (matcher.find()) {
            return new Command(
                    Integer.valueOf(matcher.group("security")),
                    Way.valueOf(matcher.group("way")),
                    Integer.valueOf(matcher.group("quantity"))
            );
        } else {
            throw new IllegalStateException("Invalid input [input = " + text + "]");
        }
    }

    private Command(int security, Way way, int quantity) {
        this.security = security;
        this.way = way;
        this.quantity = quantity;
    }

    public int getSecurity() {
        return security;
    }

    public Way getWay() {
        return way;
    }

    public int getQuantity() {
        return quantity;
    }
}
