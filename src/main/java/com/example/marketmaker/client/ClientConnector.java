package com.example.marketmaker.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientConnector {
    private final PrintWriter output;
    private final Socket socket;
    private final BufferedReader inFromClient;

    public ClientConnector(String host, int port) throws IOException {
        socket = new Socket(host, port);
        output = new PrintWriter(socket.getOutputStream());
        inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    private void startRunning() throws IOException {
        String command;
        Scanner myObj = new Scanner(System.in);
        while (!"exit".equals(command = myObj.nextLine())) {
            sendCommand(command);
        }
    }

    public double sendCommand(String command) throws IOException {
        output.println(command);
        output.flush();

        Double quotedPrice = Double.valueOf(inFromClient.readLine());
        System.out.println("Received: " + quotedPrice);

        return quotedPrice;
    }

    public static void main(String[] args) throws IOException {
        ClientConnector clientConnector = new ClientConnector("localhost", 12345);
        clientConnector.startRunning();

    }
}
