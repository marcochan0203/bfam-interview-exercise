package com.example.marketmaker.server;

import com.example.marketmaker.client.ClientConnector;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ServerApplicationTest {

    @Test
    void testThreeClientsCallingForTheSameSecurityShouldReturnBackTheSamePrice() throws Exception {
        Thread server = new Thread(() -> {
            try {
                new ServerApplication(12345);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        server.start();

        Callable<Double> client1 = () -> buildClient("123 BUY 1");
        Callable<Double> client2 = () -> buildClient("123 BUY 1");
        Callable<Double> client3 = () -> buildClient("123 BUY 1");


        Double result1 = client1.call();
        Double result2 = client2.call();
        Double result3 = client3.call();

        assertEquals(result1, result2);
        assertEquals(result1, result3);
        assertEquals(result2, result3);
    }

    @Test
    void testTwoClientsCallingForTheSameSecurityWithDifferentWayShouldHaveBuyLargerThanSell() throws Exception {
        Thread server = new Thread(() -> {
            try {
                new ServerApplication(12345);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        server.start();

        Callable<Double> client1 = () -> buildClient("123 BUY 1");
        Callable<Double> client2 = () -> buildClient("123 SELL 1");


        Double result1 = client1.call();
        Double result2 = client2.call();

        assertTrue(result1 > result2);
    }

    private Double buildClient(String command) throws IOException {
        try {
            ClientConnector clientConnector = new ClientConnector("localhost", 12345);
            return clientConnector.sendCommand(command);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }
}