package com.example.marketmaker.server.services.impl;

import com.example.marketmaker.server.services.QuoteCalculationEngine;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QuoteCalculationEngineImplTest {
    QuoteCalculationEngine quoteCalculationEngine = new QuoteCalculationEngineImpl(0.01);
    @Test
    void calculateQuotePriceForBuy() {
        double expectedReferencePrice = 1010;

        double actualReferencePrice = quoteCalculationEngine.calculateQuotePrice(123, 100, true, 10);

        assertEquals(expectedReferencePrice, actualReferencePrice);
    }
    @Test
    void calculateQuotePriceForSell() {
        double expectedReferencePrice = 990;

        double actualReferencePrice = quoteCalculationEngine.calculateQuotePrice(123, 100, false, 10);

        assertEquals(expectedReferencePrice, actualReferencePrice);
    }
}